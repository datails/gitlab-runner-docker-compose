# GitLab Runner Docker Compose Boilerplate
A boilerplate for gitlab runners using Docker Compose.

## Working:
Get a runner registration token found in `/-/settings/ci_cd` in your gitlab project.

Run the runner:

```bash
docker-compose up -d
docker exec -it $(docker ps -ql) bash
gitlab-runner register --url https://gitlab.com/ --registration-token [YOUR_GITLAB_RUNNER_TOKEN] --executor docker

# finish the questionnaire. I recommend to change the
# config.toml in the config directory with concurrency to 10
# and base image ubuntu:20.04
```

Start a pipeline.